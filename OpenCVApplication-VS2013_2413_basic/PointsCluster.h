#pragma once
#include <opencv2/opencv.hpp>
#include <iostream>


using namespace cv;

#ifndef PointsCluster_H
#define PointsCluster_H

class PointsCluster {
public:
	PointsCluster(Point clusterRepresentat);

	Point GetRepresentant();
	void UpdateRepresentant();

	Point GetPoint(int index);
	void AddPoint(Point Point);
	void RemovePoint(int index);
	int PointsSize();
	void ClearPoints();

	Vec3b GetClusterColor();

private:
	Vec3b clusterColor;
	Point representant;
	vector<Point> points;
};

#endif


