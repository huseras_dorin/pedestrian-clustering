#include "stdafx.h"
#include "ImagesCluster.h"

ImagesCluster::ImagesCluster(Image clusterRepresentat)
{
	representant = clusterRepresentat;
	Vec3b color = { (uchar)(rand() % 255),
		(uchar)(rand() % 255) ,
		(uchar)(rand() % 255) };
	clusterColor = color;
	images.push_back(clusterRepresentat);
}

Image ImagesCluster::GetRepresentant()
{
	return representant;
}

void ImagesCluster::UpdateRepresentant()
{
	for (int y = 0; y < representant.GetDistanceTransformedImage().rows; y++)
		for (int x = 0; x < 3 * representant.GetDistanceTransformedImage().cols; x++) {
			int score = 0;
			for (int i = 0; i < images.size(); i++){
				
				score += images[i].GetDistanceTransformedImage().at<uchar>(y, x);
			}
			
			representant.GetDistanceTransformedImage().at<uchar>(y, x) = score/ images.size();
		}
}
Image ImagesCluster::GetImage(int index)
{
	return images[index];
}

void ImagesCluster::AddImage(Image image)
{
	images.push_back(image);
}

void ImagesCluster::RemoveImage(int index)
{
	images.erase(images.begin() + index);

}

int ImagesCluster::ImagesNumber()
{
	return images.size();
}

void ImagesCluster::ClearImages()
{
	images.clear();
}

Vec3b ImagesCluster::GetClusterColor()
{
	return clusterColor;
}

Image ImagesCluster::FindRepresentant()
{
	int minImageIndex = 0;
	int minScore = 99999999;

	for (int i = 0; i < images.size(); i++) {
		int score = 0;
		for (int y = 0; y < representant.GetDistanceTransformedImage().rows; y++)
			for (int x = 0; x < 3 * representant.GetDistanceTransformedImage().cols; x++) 

				score += images[i].GetDistanceTransformedImage().at<uchar>(y, x);

		if (minScore > score) {
			minScore = score;
			minImageIndex = i;
		}
	}
	return images[minImageIndex];
}
