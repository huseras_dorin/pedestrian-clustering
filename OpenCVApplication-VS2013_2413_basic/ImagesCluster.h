#pragma once
#include <opencv2/opencv.hpp>
#include <iostream>
#include "Image.h"


using namespace cv;

#ifndef ImagesCluster_H
#define ImagesCluster_H

class ImagesCluster {
public:
	ImagesCluster(Image clusterRepresentat);
	int ClusterNumber;

	Image GetRepresentant();
	void UpdateRepresentant();

	Image GetImage(int index);
	void AddImage(Image image);
	void RemoveImage(int index);
	int ImagesNumber();
	void ClearImages();

	Vec3b GetClusterColor();

	Image FindRepresentant();

private:
	
	Vec3b clusterColor;
	Image representant;
	vector<Image> images;
};

#endif


