#pragma once
#include <opencv2/opencv.hpp>
#include <iostream>


using namespace cv;

#ifndef Image_H
#define Image_H

class Image {
public:
	Image(char fname[],char imName[]);
	Image();

	Mat GetImage();
	Mat GetDistanceTransformedImage();
	char GetImageName();
	Point GetImageMassCenter();
	Point GetObjectPoint(int index);
	int GetNumberOfObjectPoints();
private:

	vector<Point> objectPoints;
	void PerformDistanceTransorm();
	void SetImagePointsAndMassCenter();
	Mat image;
	Mat distnaceTransfomedImage;
	Point imageMassCenter;
	char imageName[127];
};

#endif


