// OpenCVApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "common.h"
#include "PointsCluster.h"
#include "Image.h"
#include "ImagesCluster.h"
using namespace std;


vector<Point> readPointsFromImage(Mat img) {
	vector<Point> points;

	for (int i = 0; i < img.rows; i++)
		for (int j = 0; j < img.cols; j++) {
			if (img.at<uchar>(i, j) == 0) {
				points.push_back(Point(j, i));
			}
		}
	return points;
}

Mat createWhiteImage(int height, int width) {

	Mat img(width, height, CV_8UC3);

	for (int i = 0; i < img.rows; i++)
		for (int j = 0; j < img.cols; j++) {
			Vec3b color = { 255, 255 , 255 };
			img.at<Vec3b>(j, i) = color;
		}

	return img;
}

int pointsDistance(Point point1, Point point2) {

	int xDif = point1.x - point2.x;
	int yDif = point1.y - point2.y;

	return sqrt( xDif * xDif + yDif * yDif);
}

int closestClusterIndexToPoint(Point point , vector<PointsCluster> clusters) {

	int minDistance = pointsDistance(point, clusters[0].GetRepresentant());
	int distance,index = 0;

	for (int i = 1; i < clusters.size();i++) {
		distance = pointsDistance(point, clusters[i].GetRepresentant());
		if (distance < minDistance) {
			minDistance = distance;
			index = i;
		}
	}
	return index;
}

Mat drowPointsClustersOnImage(vector<PointsCluster> clusters, Mat img) {

	int d = 6;
	for (int i = 0; i < clusters.size(); i++)
		for (int j = 0; j < clusters[i].PointsSize(); j++) {
			for (int l = -d / 2; l < d / 2; l++)
				for (int k = -d / 2; k < d / 2; k++) {
					img.at<Vec3b>(clusters[i].GetPoint(j).x + k, clusters[i].GetPoint(j).y + l) = clusters[i].GetClusterColor();
				}
		}
	return img;
}

PointsCluster mergePointsClusters(PointsCluster cluster1, PointsCluster cluster2) {

	for (int i = 0; i < cluster2.PointsSize(); i++) {
		cluster1.AddPoint(cluster2.GetPoint(i));
	}

	cluster1.UpdateRepresentant();

	return cluster1;

}

vector<PointsCluster> MBSAS(vector<Point> points ,int minDistanceBetweenClusters,int maxNumberOfClusters) {
	vector<PointsCluster> pointsClusters;
	int clustersNumber = 1;

	PointsCluster pointsCluster(points[0]);
	points.erase(points.begin());
	pointsClusters.push_back(pointsCluster);

	int index = 0;
	while (index < points.size()) {
		int k = closestClusterIndexToPoint(points[index], pointsClusters);
		if (pointsDistance(points[index], pointsClusters[k].GetRepresentant()) > minDistanceBetweenClusters && clustersNumber < maxNumberOfClusters) {
			PointsCluster pointsCluster(points[index]);

			points.erase(points.begin() + index);
			pointsClusters.push_back(pointsCluster);
			clustersNumber++;
		}
		else {
			index++;
		}
	}

	for (int i = 0;i < points.size(); i++) {
		int k = closestClusterIndexToPoint(points[i], pointsClusters);
		pointsClusters[k].AddPoint(points[i]);
		pointsClusters[k].UpdateRepresentant();
	}

	return pointsClusters;
}

vector<PointsCluster> refinement(vector<PointsCluster> clusters, int minDistanceBetweenClustersForRefinement) {

	int semaphore = 1;

	while (semaphore && clusters.size() > 1) {
		int minDistance = pointsDistance(clusters[0].GetRepresentant(), clusters[1].GetRepresentant());
		int peari = 0, pearj = 1;
		semaphore = false;
		for (int i = 0; i < clusters.size() - 1; i++) {
			for (int j = i + 1; j < clusters.size(); j++) {
				int distance = pointsDistance(clusters[i].GetRepresentant(), clusters[j].GetRepresentant());
				if (distance < minDistance) {
					minDistance = distance;
					peari = i;
					pearj = j;
				}
			}
		}

		if (minDistance <= minDistanceBetweenClustersForRefinement) {
			semaphore = 1;
			clusters[peari] = mergePointsClusters(clusters[peari], clusters[pearj]);
			clusters.erase(clusters.begin() + pearj);
		}

	}
	return clusters;
}

vector<PointsCluster> reassignment(vector<Point> points, vector<PointsCluster> clusters) {
	vector<int> reassignments;

	for (int i = 0; i < points.size(); i++) {
		int closestCluster = closestClusterIndexToPoint(points[i], clusters);
		reassignments.push_back(closestCluster);
	}

	for (int i = 0; i < clusters.size(); i++) {
		clusters[i].ClearPoints();
	}
	
	for (int i = 0; i < reassignments.size(); i++) {
		clusters[reassignments[i]].AddPoint(points[i]);
	}

	return clusters;
}

void pointsClustering() {
	vector<Point> imagePoints;
	vector<PointsCluster> pointsClusters;
	int minDistanceBetweenClusterForRefinement, minDistanceBetweenClusters, maxNumberOfClusters;
	bool merge = true;
	Mat img2;
	Mat img = imread("Lab6/points1.bmp", IMREAD_GRAYSCALE);
	
	imagePoints = readPointsFromImage(img);

	// MBSAS stage 

	cout << "Teta  pentru MBSAS = ";
	cin >> minDistanceBetweenClusters;
	cout << "Pragul q pentru  numarul de clustere = ";
	cin >> maxNumberOfClusters;
	pointsClusters = MBSAS(imagePoints, minDistanceBetweenClusters, maxNumberOfClusters);

	img2 = createWhiteImage(img.rows, img.cols);
	drowPointsClustersOnImage(pointsClusters , img2);
	imshow("Image after MBSAS", img2);
	waitKey();

	// end MBSAS stage

	// refinement stage 

	cout << "Pragul M pentru  stagiul de rafinare = ";
	cin >> minDistanceBetweenClusterForRefinement;
	pointsClusters = refinement(pointsClusters, minDistanceBetweenClusterForRefinement);

	img2 = createWhiteImage(img.rows, img.cols);
	drowPointsClustersOnImage(pointsClusters, img2);
	imshow("Image after refinement ", img2);
	waitKey();

	// end refinement stage

	// reassignment stage 
	
	pointsClusters = reassignment(imagePoints, pointsClusters);

	img2 = createWhiteImage(img.rows, img.cols);
	drowPointsClustersOnImage(pointsClusters, img2);
	imshow("Image after reassignment ", img2);
	waitKey();
	// end reassignment stage
}

/////////////////////
/// Pedestrian 
////////////////////

vector<Image> loadImages(int instances) {
	vector<Image> images;
	char fname[MAX_PATH];
	char imageName[MAX_PATH];

	for (int i = 1; i <= instances; i++) {
		sprintf(fname, "templates/t%dzoom.bmp", i);
		sprintf(imageName, "t%dzoom.bmp", i);
		Image image(fname,imageName);
		images.push_back(image);
		waitKey();
	}
	return images;
}

void printClusters(vector<ImagesCluster> clusters, int iteration) {
	char savePath[MAX_PATH];

	for (int i = 0; i < clusters.size(); i++) {
		sprintf(savePath, "Results/Cluster%d_%d/Representant.bmp", iteration, clusters[i].ClusterNumber);
		imwrite(savePath, clusters[i].FindRepresentant().GetImage());
		for (int j = 0; j < clusters[i].ImagesNumber(); j++) {
			sprintf(savePath, "Results/Cluster%d_%d/%d.bmp", iteration, clusters[i].ClusterNumber ,j);
			imwrite(savePath, clusters[i].GetImage(j).GetImage());	
		}
	}	
}

int distanceTransformScore(Image image1 , Image image2) {
	double score = 0;
	int numberofpoints = 0;

	for (int i = 0; i < image1.GetNumberOfObjectPoints(); i++) {

		int x = image1.GetObjectPoint(i).x - image1.GetImageMassCenter().x + image2.GetImageMassCenter().x;
		int y = image1.GetObjectPoint(i).y - image1.GetImageMassCenter().y + image2.GetImageMassCenter().y;
		if (x > 0 && y > 0 && x < 3 * image2.GetDistanceTransformedImage().cols && y <  image2.GetDistanceTransformedImage().rows) {
			numberofpoints++;
			score += image2.GetDistanceTransformedImage().at<uchar>(y, x);
		}	
	}
	score = score / numberofpoints;
	return score;
}

int closestClusterIndexToImage(Image image, vector<ImagesCluster> clusters) {

	int minDistance = distanceTransformScore(image, clusters[0].GetRepresentant());
	int distance, index = 0;

	for (int i = 1; i < clusters.size(); i++) {
		distance = distanceTransformScore(image, clusters[i].GetRepresentant());
		if (distance < minDistance) {
			minDistance = distance;
			index = i;
		}
	}
	return index;
}

ImagesCluster mergeImagesClusters(ImagesCluster cluster1, ImagesCluster cluster2) {

	for (int i = 0; i < cluster2.ImagesNumber(); i++) {
		cluster1.AddImage(cluster2.GetImage(i));
	}

	cluster1.UpdateRepresentant();

	return cluster1;

}

vector<ImagesCluster> MBSAS(vector<Image> images, int minDistanceBetweenClusters, int maxNumberOfClusters) {
	vector<ImagesCluster> imagesClusters;
	int clustersNumber = 0;

	ImagesCluster imagesCluster(images[0]);
	imagesCluster.ClusterNumber = clustersNumber;
	images.erase(images.begin());
	imagesClusters.push_back(imagesCluster);

	clustersNumber++;

	int index = 0;
	while (index < images.size()) {
		int k = closestClusterIndexToImage(images[index], imagesClusters);
		if (distanceTransformScore(images[index], imagesClusters[k].GetRepresentant()) > minDistanceBetweenClusters && clustersNumber < maxNumberOfClusters) {
			ImagesCluster imagesCluster(images[index]);
			imagesCluster.ClusterNumber = clustersNumber;

			images.erase(images.begin() + index);
			imagesClusters.push_back(imagesCluster);
			clustersNumber++;
		}
		else {
			index++;
		}
	}
	for (int i = 0; i < images.size(); i++) {
		int k = closestClusterIndexToImage(images[i], imagesClusters);
		imagesClusters[k].AddImage(images[i]);
		imagesClusters[k].UpdateRepresentant();
	}

	return imagesClusters;
}

vector<ImagesCluster> refinement(vector<ImagesCluster> clusters, int minDistanceBetweenClustersForRefinement) {

	int semaphore = 1;

	while (semaphore && clusters.size() > 1) {
		int minDistance = distanceTransformScore(clusters[0].GetRepresentant(), clusters[1].GetRepresentant());
		int peari = 0, pearj = 1;
		semaphore = false;
		for (int i = 0; i < clusters.size() - 1; i++) {
			for (int j = i + 1; j < clusters.size(); j++) {
				int distance = distanceTransformScore(clusters[i].GetRepresentant(), clusters[j].GetRepresentant());
				if (distance < minDistance) {
					minDistance = distance;
					peari = i;
					pearj = j;
				}
			}
		}

		if (minDistance <= minDistanceBetweenClustersForRefinement) {
			semaphore = 1;
			clusters[peari] = mergeImagesClusters(clusters[peari], clusters[pearj]);
			clusters.erase(clusters.begin() + pearj);
		}

	}
	return clusters;
}

vector<ImagesCluster> reassignment(vector<Image> images, vector<ImagesCluster> clusters) {
	vector<int> reassignments;

	for (int i = 0; i < images.size(); i++) {
		int closestCluster = closestClusterIndexToImage(images[i], clusters);
		reassignments.push_back(closestCluster);
	}

	for (int i = 0; i < clusters.size(); i++) {
		clusters[i].ClearImages();
	}

	for (int i = 0; i < reassignments.size(); i++) {
		clusters[reassignments[i]].AddImage(images[i]);
	}

	for (int i = 0; i < clusters.size(); i++) {
		clusters[i].UpdateRepresentant();
	}

	return clusters;
}

vector<Image> loadRepresentantsFromClusters(vector<ImagesCluster> clusters) {
	vector<Image> images;

	for (int i = 0; i < clusters.size(); i++) {
		images.push_back(clusters[i].FindRepresentant());
	}

	return images;
}

Mat drowTreeImnage(vector<ImagesCluster> clusters, Mat groups , int i) {

	int j = 0;
	for (int k = 0; k < clusters.size(); k++) {
		for (int l = 0; l < clusters[k].ImagesNumber(); l++) {
			Mat interm;
			resize(clusters[k].GetImage(l).GetImage(), interm, Size(20, 20),(0.0),(0.0),3);
			Mat dst = groups(Rect(j, i, interm.cols, interm.rows));
			interm.copyTo(dst);
			j += 20;
		}
		if (j < groups.cols / 4) {
			j += 30;
		}
		else {
			j = 0;
			i += 50;
		}
	}

	imshow("clusters", groups);
	waitKey();

	return groups;
}

void pedestrianClustering() {
	vector<Image> images = loadImages(128);
	vector<ImagesCluster> imagesClusters;
	int minDistanceBetweenClusters = 10, maxNumberOfClusters = 19, minDistanceBetweenClusterForRefinement = 14;

	/*cout << "Teta  pentru MBSAS = ";
	cin >> minDistanceBetweenClusters;
	cout << "Pragul q pentru  numarul de clustere = ";
	cin >> maxNumberOfClusters;
	cout << "Pragul M pentru  stagiul de rafinare = ";
	cin >> minDistanceBetweenClusterForRefinement;
	int iteration = 0;
	*/
	int height = 300, width = 20000;
	Mat groups(height, width, CV_8UC3);

	int i = 0;

	for (int i = 0; i < groups.rows; i++) {
		for (int j = 0; j < groups.cols; j++) {
			groups.at<Vec3b>(i, j)[0] = 255;
			groups.at<Vec3b>(i, j)[1] = 255;
			groups.at<Vec3b>(i, j)[2] = 255;
		}
	}


	while (images.size()>=1 && maxNumberOfClusters  > 0) {

		imagesClusters = MBSAS(images, minDistanceBetweenClusters, maxNumberOfClusters);

		imagesClusters = refinement(imagesClusters, minDistanceBetweenClusterForRefinement);

		imagesClusters = reassignment(images, imagesClusters);

		minDistanceBetweenClusters *= 2;
		maxNumberOfClusters /= 2;
		minDistanceBetweenClusterForRefinement *= 2;

		drowTreeImnage(imagesClusters, groups,i);
		i += 50;

		//printClusters(imagesClusters, iteration);
		//iteration++;
		images = loadRepresentantsFromClusters(imagesClusters);
	}


}

int main()
{
	int op;
	do
	{
		system("cls");
		destroyAllWindows();

		printf("Menu:\n");

		printf(" 1 - Points Clustering \n");
		printf(" 2 - Pedestrian Clustering \n");
		printf(" 0 - Exit\n\n");
		printf("Option: ");

		scanf("%d",&op);
		switch (op)
		{
			case 1 : pointsClustering();
			break;
			case 2 : pedestrianClustering();
			break;
		}
	}
	while (op!=0);
	return 0;
}