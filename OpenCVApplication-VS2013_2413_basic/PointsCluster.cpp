#include "stdafx.h"
#include "PointsCluster.h"


PointsCluster::PointsCluster(Point clusterRepresentat)
{
	representant = clusterRepresentat;
	Vec3b color = { (uchar)(rand() % 255),
		(uchar)(rand() % 255) ,
		(uchar)(rand() % 255) };
	clusterColor = color;
	points.push_back(clusterRepresentat);
}

Point PointsCluster::GetRepresentant() { 
	return representant;
}

void PointsCluster::UpdateRepresentant() {

	int x = 0, y = 0;

	for (int i = 0; i < points.size(); i++) {
		x += points[i].x;
		y += points[i].y;
	}

	representant = Point(x / points.size(), y / points.size());
}

void PointsCluster::RemovePoint(int index)
{
	points.erase(points.begin() + index);
}

int PointsCluster::PointsSize()
{
	return points.size();
}

void PointsCluster::ClearPoints()
{
	points.clear();
}

Point PointsCluster::GetPoint(int index)
{
	return points[index];
}

void PointsCluster::AddPoint(Point Point)
{
	points.push_back(Point);
}

Vec3b PointsCluster::GetClusterColor()
{
	return clusterColor;
}
