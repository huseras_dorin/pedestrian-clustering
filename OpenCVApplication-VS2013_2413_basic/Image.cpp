#include "stdafx.h"
#include "Image.h"

Image::Image(char fname[],char imName[])
{
	image = imread(fname);
	PerformDistanceTransorm();
	strcpy(imageName, imName);
	SetImagePointsAndMassCenter();
}

Image::Image()
{
}

Mat Image::GetImage()
{
	return image;
}

Mat Image::GetDistanceTransformedImage()
{
	return distnaceTransfomedImage;
}

char Image::GetImageName()
{
	return imageName[127];
}

Point Image::GetImageMassCenter()
{
	return imageMassCenter;
}

int Image::GetNumberOfObjectPoints() {

	return objectPoints.size();
}

Point Image::GetObjectPoint(int index)
{
	return objectPoints[index];
}

void Image::PerformDistanceTransorm()
{
	int di[] = { -1 , -1 , -1 , 0 , 0 , 1, 1, 1 };
	int dj[] = { -1 , 0,  1 , -1 , 1 , -1 , 0,  1 };
	int weight[] = { 3, 2, 3, 2, 2, 3, 2, 3 };
	int kernelSize = 8;
	distnaceTransfomedImage = image.clone();


	for (int i = 1; i < distnaceTransfomedImage.rows - 1; i++)
		for (int j = 1; j < distnaceTransfomedImage.cols * 3 - 1; j++)
			for (int k = 0; k < kernelSize; k++)
				if (distnaceTransfomedImage.at<uchar>(i + di[k], j + dj[k]) + weight[k] < distnaceTransfomedImage.at<uchar>(i, j))
					distnaceTransfomedImage.at<uchar>(i, j) = distnaceTransfomedImage.at<uchar>(i + di[k], j + dj[k]) + weight[k];

	for (int i = distnaceTransfomedImage.rows - 2; i > 0; i--)
		for (int j = distnaceTransfomedImage.cols * 3 - 2; j > 0; j--)
			for (int k = 0; k < kernelSize; k++)
				if (distnaceTransfomedImage.at<uchar>(i + di[k], j + dj[k]) + weight[k] < distnaceTransfomedImage.at<uchar>(i, j))
					distnaceTransfomedImage.at<uchar>(i, j) = distnaceTransfomedImage.at<uchar>(i + di[k], j + dj[k]) + weight[k];
}

void Image::SetImagePointsAndMassCenter()
{
	double x = 0 , y = 0;
	int points = 0;
	for (int i = 1; i < image.rows ; i++)
		for (int j = 1; j < image.cols * 3 ; j++)
			if (image.at<uchar>(i, j) == 0) {
				x += j;
				y += i;
				points ++;
				objectPoints.push_back(Point(j, i));
			}
	imageMassCenter = Point(x / points, y / points);
}
